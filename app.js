if (process.env.ENV_FILE) {
    require('dotenv').config({ path: `.env.${process.env.ENV_FILE}` });
} else {
    require('dotenv').config();
}

const  cors = require('cors')
const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const PORT = process.env.PORT || 7070;
const indexRouter = require('./routes/index');
const tokoRouter = require('./routes/toko');
const statusRouter = require('./routes/status');
const typeRouter = require('./routes/type');
const addressRouter = require('./routes/address');
const daerahRouter = require('./routes/daerah');

const app = express();
app.use(cors())
 
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/', indexRouter);
app.use('/api/v1/status', statusRouter);
app.use('/api/v1/type', typeRouter);
app.use('/api/v1/toko', tokoRouter);
app.use('/api/v1/address', addressRouter);
app.use('/api/v1/daerah/indonesia', daerahRouter);

app.listen(PORT, () => {
    console.log("Server Runing On Port: ", PORT);
});


