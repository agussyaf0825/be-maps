exports.ok = async (value, res) => {
    const data = {
        'success': true,
        'data': value
    }
    res.json(data);
    res.end();
}

exports.fail = async (kode, value, res) => {
    const data = {
        'success': false,
        'message': value
    }
    res.status(kode).json(data);
    res.end();
}