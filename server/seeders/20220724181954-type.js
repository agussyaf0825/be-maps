'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('types', [
      {
        name: "utama",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: "cabang",
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('types', null, {});
  }
};
