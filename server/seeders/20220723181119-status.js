'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('statuses', [
      {
        name: "active",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: "not active",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: "blocked",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: "not verify",
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('statuses', null, {});
  }
};
