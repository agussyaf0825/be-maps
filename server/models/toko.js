'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class toko extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      toko.hasOne(models.status, { foreignKey: 'id', sourceKey: 'status', as: "statuses" });
      toko.hasMany(models.address, { foreignKey: 'tokoUuid', sourceKey: 'tokoUuid', onDelete: 'RESTRICT', onUpdate: 'NO ACTION' });
      // define association here
    }
  }
  toko.init({
    tokoUuid: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 },
    name: DataTypes.STRING,
    status: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'toko',
  });
  return toko;
};