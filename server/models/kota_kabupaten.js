'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class kota_kabupaten extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  kota_kabupaten.init({
    provinsi_id: DataTypes.INTEGER,
    name: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'kota_kabupaten',
    indexes: [
      {
        unique: false,
        fields: ['provinsi_id'],
      }
    ]
  });
  return kota_kabupaten;
};