'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class address extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      address.hasOne(models.status, { foreignKey: 'id', sourceKey: 'status', as: "statuses" });
      // define association here
    }
  }
  address.init({
    tokoUuid: DataTypes.UUID,
    addressUuid: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 },
    name: DataTypes.STRING,
    phone: DataTypes.STRING,
    address: DataTypes.TEXT,
    provinsi_id: DataTypes.CHAR(2),
    kota_kabupaten_id: DataTypes.CHAR(4),
    kecamatan_id: DataTypes.CHAR(7),
    kelurahan_id: DataTypes.CHAR(10),
    zip_code: DataTypes.STRING,
    latitude: DataTypes.STRING,
    longitude: DataTypes.STRING,
    type: DataTypes.INTEGER,
    status: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'address',
    indexes: [
      {
        unique: false,
        fields: ['tokoUuid', 'addressUuid', 'latitude', 'longitude', 'type', 'status'],
      }
    ]
  });
  return address;
};