'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();
    await queryInterface.createTable('tokos', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      tokoUuid: {
        type: Sequelize.UUID,
        unique: true
      },
      name: {
        type: Sequelize.STRING
      },
      status: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
    await queryInterface.addConstraint('addresses', {
      fields: ["tokoUuid"],
      type: 'foreign key',
      name: 'custom_fkey_tokos',
      references: {
        table: 'tokos',
        field: 'tokoUuid'
      },
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE'
    });
    await queryInterface.addConstraint('tokos', {
      fields: ["status"],
      type: 'foreign key',
      name: 'custom_fkey_statuses',
      references: {
        table: 'statuses',
        field: 'id'
      },
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE'
    });
    await queryInterface.addIndex(
      'tokos',
      ['tokoUuid', 'name', 'status'],
      {
        fields: ['tokoUuid', 'name', 'status'],
        transaction,
      }
    );
    await transaction.commit();
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('tokos');
  }
};