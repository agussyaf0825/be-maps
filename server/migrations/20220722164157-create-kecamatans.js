'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();
    await queryInterface.createTable('kecamatans', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.CHAR(7)
      },
      kota_kabupaten_id: {
        type: Sequelize.CHAR(4)
      },
      name: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
    await queryInterface.addConstraint('kecamatans', {
      fields: ["kota_kabupaten_id"],
      type: 'foreign key',
      name: 'custom_key_kota_kabupaten',
      references: {
        table: 'kota_kabupatens',
        field: 'id'
      },
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE'
    });
    await queryInterface.addIndex(
      'kecamatans',
      ["kota_kabupaten_id"],
      {
        fields: ["kota_kabupaten_id"],
        transaction,
      }
    );
    await transaction.commit();
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('kecamatans');
  }
};