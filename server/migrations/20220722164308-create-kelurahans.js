'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();
    await queryInterface.createTable('kelurahans', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.CHAR(10)
      },
      kecamatan_id: {
        type: Sequelize.CHAR(7)
      },
      name: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
    await queryInterface.addConstraint('kelurahans', {
      fields: ["kecamatan_id"],
      type: 'foreign key',
      name: 'custom_key_kecamatan',
      references: {
        table: 'kecamatans',
        field: 'id'
      },
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE'
    });
    await queryInterface.addIndex(
      'kelurahans',
      ["kecamatan_id"],
      {
        fields: ["kecamatan_id"],
        transaction,
      }
    );
    await transaction.commit();
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('kelurahans');
  }
};