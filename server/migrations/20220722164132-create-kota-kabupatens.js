'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();
    await queryInterface.createTable('kota_kabupatens', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.CHAR(4)
      },
      provinsi_id: {
        type: Sequelize.CHAR(2)
      },
      name: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
    await queryInterface.addConstraint('kota_kabupatens', {
      fields: ["provinsi_id"],
      type: 'foreign key',
      name: 'custom_key_provinsi',
      references: {
        table: 'provinsis',
        field: 'id'
      },
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE'
    });
    await queryInterface.addIndex(
      'kota_kabupatens',
      ["provinsi_id"],
      {
        fields: ["provinsi_id"],
        transaction,
      }
    );
    await transaction.commit();
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('kota_kabupatens');
  }
};