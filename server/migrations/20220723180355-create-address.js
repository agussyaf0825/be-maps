'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();
    await queryInterface.createTable('addresses', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      tokoUuid: {
        type: Sequelize.UUID
      },
      addressUuid: {
        type: Sequelize.UUID,
        unique: true
      },
      name: {
        type: Sequelize.STRING
      },
      phone: {
        type: Sequelize.STRING
      },
      address: {
        type: Sequelize.TEXT
      },
      provinsi_id: {
        type: Sequelize.CHAR(2)
      },
      kota_kabupaten_id: {
        type: Sequelize.CHAR(4)
      },
      kecamatan_id: {
        type: Sequelize.CHAR(7)
      },
      kelurahan_id: {
        type: Sequelize.CHAR(10)
      },
      zip_code: {
        type: Sequelize.STRING
      },
      latitude: {
        type: Sequelize.STRING
      },
      longitude: {
        type: Sequelize.STRING
      },
      type: {
        type: Sequelize.INTEGER
      },
      status: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
    await queryInterface.addConstraint('addresses', {
      fields: ["status"],
      type: 'foreign key',
      name: 'custom_fkey_status',
      references: {
        table: 'statuses',
        field: 'id'
      },
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE'
    });
    await queryInterface.addConstraint('addresses', {
      fields: ["provinsi_id"],
      type: 'foreign key',
      name: 'custom_fkey_provinsi',
      references: {
        table: 'provinsis',
        field: 'id'
      },
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE'
    });
    await queryInterface.addConstraint('addresses', {
      fields: ["kota_kabupaten_id"],
      type: 'foreign key',
      name: 'custom_fkey_kota_kabupaten',
      references: {
        table: 'kota_kabupatens',
        field: 'id'
      },
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE'
    });
    await queryInterface.addConstraint('addresses', {
      fields: ["kecamatan_id"],
      type: 'foreign key',
      name: 'custom_fkey_kecamatan',
      references: {
        table: 'kecamatans',
        field: 'id'
      },
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE'
    });
    await queryInterface.addConstraint('addresses', {
      fields: ["kelurahan_id"],
      type: 'foreign key',
      name: 'custom_fkey_kelurahan',
      references: {
        table: 'kelurahans',
        field: 'id'
      },
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE'
    });
    await queryInterface.addConstraint('addresses', {
      fields: ["status"],
      type: 'foreign key',
      name: 'custom_key_statuses',
      references: {
        table: 'statuses',
        field: 'id'
      },
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE'
    });
    await queryInterface.addConstraint('addresses', {
      fields: ["type"],
      type: 'foreign key',
      name: 'custom_fkey_types',
      references: {
        table: 'types',
        field: 'id'
      },
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE'
    });
    await queryInterface.addIndex(
      'addresses',
      ['tokoUuid', 'addressUuid', 'latitude', 'longitude', 'type', 'status'],
      {
        fields: ['tokoUuid', 'addressUuid', 'latitude', 'longitude', 'type', 'status'],
        transaction,
      }
    );
    await transaction.commit();
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('addresses');
  }
};