const getAllkelurahan = require("./getAllkelurahan")
const getIdkelurahan = require("./getIdkelurahan")

module.exports = {
    getAllkelurahan,
    getIdkelurahan
}