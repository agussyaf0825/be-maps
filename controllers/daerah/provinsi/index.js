const getAllProvinsi = require("./getAllProvinsi")
const getIdProvinsi = require("./getIdProvinsi")

module.exports = {
    getAllProvinsi,
    getIdProvinsi
}