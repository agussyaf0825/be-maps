const getAllKecamatan = require("./getAllKecamatan")
const getIdKecamatan = require("./getIdKecamatan")

module.exports = {
    getAllKecamatan,
    getIdKecamatan
}