const Respon = require('../../../response')
const { kota_kabupaten } = require('../../../server/models')
const { Op } = require('sequelize')
module.exports = async (req, res) => {
    try {
        let page = 1
        let limit = 100
        let order = 'ASC'
        let result
        const pages = Number.parseInt(req.query.page)
        const limits = Number.parseInt(req.query.limit)
        const orders = req.query.order
        const provinsi_id = req.query.provinsi_id

        if (!Number.isNaN(pages) && pages > 1) {
            page = pages
        }
        if (!Number.isNaN(limits) && limits > 0 && limits <= 1000) {
            limit = limits
        }

        if (typeof orders !== "undefined" && orders) {
            const Order = orders.toUpperCase()
            if (Order === 'ASC') {
                order = Order
            }
            else if (Order === "DESC") {
                order = Order
            }
        }
        try {
            result = await kota_kabupaten.findAndCountAll({
                limit,
                offset: limit * page - limit,
                order: [
                    ['id', order]
                ],
                where: {
                    [Op.and]: [
                        provinsi_id && { provinsi_id: JSON.parse(provinsi_id) },
                    ],
                },
                attributes: { exclude: ['createdAt', 'updatedAt'] },
            })
        } catch (error) {
            result = await kota_kabupaten.findAndCountAll({
                limit,
                offset: limit * page - limit,
                order: [
                    ['id', order]
                ],
                where: {
                    [Op.and]: [
                        provinsi_id && { provinsi_id },
                    ],
                },
                attributes: { exclude: ['createdAt', 'updatedAt'] },
            })
        }
        const total_page = Math.ceil(result.count / Number(limit))
        const total_rows_page = result.rows.length
        result = Object.assign({}, { page, total_page, count: result.count, total_rows_page }, { rows: result.rows })

        return Respon.ok(result, res)
    } catch (error) {
        console.log(error)
        return Respon.fail(500, error.name, res)
    }
}