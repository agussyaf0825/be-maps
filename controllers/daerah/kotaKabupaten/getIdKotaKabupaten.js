const Respon = require('../../../response')
const { kota_kabupaten } = require('../../../server/models')
module.exports = async (req, res) => {
    try {
        const id = req.params.id
        const result = await kota_kabupaten.findByPk(id, {
            attributes: { exclude: ['createdAt', 'updatedAt'] },
        })
        if (!result) {
            return Respon.fail(404, "Data Not Found", res)
        }
        return Respon.ok(result, res)
    } catch (error) {
        console.log(error)
        return Respon.fail(500, error.name, res)
    }
}