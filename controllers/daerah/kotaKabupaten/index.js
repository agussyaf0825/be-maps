const getAllKotaKabupaten = require("./getAllKotaKabupaten")
const getIdKotaKabupaten = require("./getIdKotaKabupaten")

module.exports = {
    getAllKotaKabupaten,
    getIdKotaKabupaten
}