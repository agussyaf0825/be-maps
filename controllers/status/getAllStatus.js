const Respon = require('../../response')
const { status } = require('../../server/models')
module.exports = async (req, res) => {
    try {
        let result
        result = await status.findAll({
            attributes: { exclude: ['createdAt', 'updatedAt'] },
        })

        return Respon.ok(result, res)
    } catch (error) {
        console.log(error)
        return Respon.fail(500, error.name, res)
    }
}