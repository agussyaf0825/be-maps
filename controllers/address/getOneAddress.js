const Respon = require('../../response')
const { address } = require('../../server/models')
const { validate: uuidValidate } = require('uuid');
module.exports = async (req, res) => {
    try {
        let result
        const addressUuid = req.params.addressUuid
        if (!uuidValidate(addressUuid)) {
            return Respon.fail(400, "addressUuid Format Wrong", res)
        }
        result = await address.findAll({
            where: { addressUuid },
        })

        if (result.length === 0) {
            return Respon.fail(400, "Data Not Found", res)
        }
        return Respon.ok(...result, res)
    } catch (error) {
        console.log(error)
        return Respon.fail(500, error.name, res)
    }
}