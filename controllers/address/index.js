const createAddress = require("./createAddress")
const deleteAddress = require("./deleteAddress")
const getAllAddress = require("./getAllAddress")
const getOneAddress = require("./getOneAddress")
const updateAddress = require("./updateAddress")
module.exports ={
    createAddress,
    deleteAddress,
    getAllAddress,
    getOneAddress,
    updateAddress
}