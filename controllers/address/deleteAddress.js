const Respon = require('../../response')
const { address } = require('../../server/models')
const { validate: uuidValidate } = require('uuid');
module.exports = async (req, res) => {
    try {
        let result
        const addressUuid = req.params.addressUuid
        if (!uuidValidate(addressUuid)) {
            return Respon.fail(400, "addressUuid Format Wrong", res)
        }
        result = await address.findOne({ where: { addressUuid } })
        if (!result) {
            return Respon.fail(404, "addressUuid Not Found", res)
        }
        await address.destroy({ where: { addressUuid } })

        const dataRes = {
            message: "Address Deleted Success",
            addressUuid,
        }

        return Respon.ok(dataRes, res)
    } catch (error) {
        console.log(error)
        return Respon.fail(500, error.name, res)
    }
}