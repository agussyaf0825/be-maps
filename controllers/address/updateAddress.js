const Respon = require('../../response')
const { address } = require('../../server/models')
const { validate: uuidValidate } = require('uuid');
module.exports = async (req, res) => {
    try {
        const addressUuid = req.params.addressUuid
        if (!uuidValidate(addressUuid)) {
            return Respon.fail(400, "addressUuid Format Wrong", res)
        }
        result = await address.findOne({ where: { addressUuid } })
        if (!result) {
            return Respon.fail(404, "Address Not Found", res)
        }
        await address.update(req.body, { where: { addressUuid } })
        const datares = {
            message: "Updated Address Success",
            addressUuid
        }
        return Respon.ok(datares, res)
    } catch (error) {
        console.log(error)
        return Respon.fail(500, error.name, res)
    }
}