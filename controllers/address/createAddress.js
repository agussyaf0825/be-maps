const Respon = require('../../response')
const Validator = require('fastest-validator');
const v = new Validator();
const { toko, address } = require('../../server/models')

module.exports = async (req, res) => {
    try {
        let result
        const schema = {
            tokoUuid: 'uuid|empty:false',
            name: 'string|empty:false',
            phone: 'string|empty:false',
            address: 'string|empty:false',
            provinsi_id: 'string|empty:false',
            kota_kabupaten_id: 'string|empty:false',
            kecamatan_id: 'string|empty:false',
            kelurahan_id: 'string|empty:false',
            zip_code: 'string|empty:false',
            latitude: 'string|empty:false',
            longitude: 'string|empty:false',
            type: 'number|empty:false',
            status: 'number|empty:false',
        }
        const validate = v.validate(req.body, schema);
        if (validate.length) {
            return Respon.fail(400, validate, res);
        }
        const { tokoUuid } = req.body
        result = await toko.findOne({ where: { tokoUuid } })
        if (!result) {
            return Respon.fail(404, "Toko Not Found", res)
        }
        result = await address.create(req.body)

        const dataReq = {
            message: "Add New Address SuccessFull",
            addressUuid: result.addressUuid
        }
        return Respon.ok(dataReq, res)
    } catch (error) {
        console.log(error)
        return Respon.fail(500, error.name, res)
    }
}