const Respon = require('../../response')
const { toko, address } = require('../../server/models')
const { validate: uuidValidate } = require('uuid');
module.exports = async (req, res) => {
    try {
        let result
        const tokoUuid = req.params.tokoUuid
        if (!uuidValidate(tokoUuid)) {
            return Respon.fail(400, "tokoUuid Format Wrong", res)
        }

        result = await toko.findAll({
            include: [{ model: address, }],
            where: { tokoUuid },
        })

        if (result.length === 0) {
            return Respon.fail(400, "Data Not Found", res)
        }
        return Respon.ok(...result, res)
    } catch (error) {
        console.log(error)
        return Respon.fail(500, error.name, res)
    }
}