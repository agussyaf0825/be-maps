const Respon = require('../../response')
const Validator = require('fastest-validator');
const v = new Validator();
const { toko, address } = require('../../server/models')

module.exports = async (req, res) => {
    try {
        let result
        const schema = {
            name: 'string|empty:false',
            status: 'number|empty:false',
            address: {
                type: "array", items: {
                    type: "object", props: {
                        name: 'string|empty:false',
                        phone: 'string|empty:false',
                        address: 'string|empty:false',
                        provinsi_id: 'string|empty:false',
                        kota_kabupaten_id: 'string|empty:false',
                        kecamatan_id: 'string|empty:false',
                        kelurahan_id: 'string|empty:false',
                        zip_code: 'string|empty:false',
                        latitude: 'string|empty:false',
                        longitude: 'string|empty:false',
                        type: 'number|empty:false',
                        status: 'number|empty:false',
                    }
                }
            }
        }

        const validate = v.validate(req.body, schema);
        if (validate.length) {
            return Respon.fail(400, validate, res);
        }
        const { name, status, } = req.body
        result = await toko.create({ name, status, })
        await address.create(Object.assign({}, ...req.body.address, { tokoUuid: result.tokoUuid }))
        const dataReq = {
            message: "Add New Toko SuccessFull",
            tokoUuid: result.tokoUuid
        }
        return Respon.ok(dataReq, res)
    } catch (error) {
        console.log(error)
        return Respon.fail(500, error.name, res)
    }
}