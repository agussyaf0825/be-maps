const Respon = require('../../response')
const { toko, } = require('../../server/models')
const { validate: uuidValidate } = require('uuid');
module.exports = async (req, res) => {
    try {
        const tokoUuid = req.params.tokoUuid
        if (!uuidValidate(tokoUuid)) {
            return Respon.fail(400, "tokoUuid Format Wrong", res)
        }
        result = await toko.findOne({ where: { tokoUuid } })
        if (!result) {
            return Respon.fail(404, "Toko Not Found", res)
        }
        
        await toko.update(req.body, { where: { tokoUuid } })
        const datares = {
            message: "Updated Toko Success",
            tokoUuid
        }
        return Respon.ok(datares, res)
    } catch (error) {
        console.log(error)
        return Respon.fail(500, error.name, res)
    }
}