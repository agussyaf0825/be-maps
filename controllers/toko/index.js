const createToko = require("./createToko")
const deleteToko = require("./deleteToko")
const getAllToko = require("./getAllToko")
const getOneToko = require("./getOneToko")
const updateToko = require("./updateToko")
module.exports ={
    createToko,
    deleteToko,
    getAllToko,
    getOneToko,
    updateToko
}