const Respon = require('../../response')
const { toko, } = require('../../server/models')
const { validate: uuidValidate } = require('uuid');
module.exports = async (req, res) => {
    try {
        let result
        const tokoUuid = req.params.tokoUuid
        if (!uuidValidate(tokoUuid)) {
            return Respon.fail(400, "tokoUuid Format Wrong", res)
        }
        result = await toko.findOne({ where: { tokoUuid } })
        if (!result) {
            return Respon.fail(404, "Toko Not Found", res)
        }
        await toko.destroy({ where: { tokoUuid } })
        
        const dataRes = {
            message: "Toko Deleted Success",
            tokoUuid,
        }

        return Respon.ok(dataRes, res)
    } catch (error) {
        console.log(error)
        return Respon.fail(500, error.name, res)
    }
}