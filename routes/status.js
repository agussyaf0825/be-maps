const express = require('express');
const router = express.Router();
const { getAllStatus } = require('../controllers/status')

/* GET users listing. */
router.get('/', getAllStatus);

module.exports = router;
