const express = require('express');
const router = express.Router();
const { createToko, deleteToko, getAllToko, getOneToko, updateToko } = require('../controllers/toko')

/* GET users listing. */
router.get('/', getAllToko);
router.get('/:tokoUuid', getOneToko);
router.post('/', createToko);
router.put('/:tokoUuid', updateToko);
router.delete('/:tokoUuid', deleteToko);

module.exports = router;
