const express = require('express');
const router = express.Router();
const { createAddress, deleteAddress, getAllAddress, getOneAddress, updateAddress } = require('../controllers/address')

/* GET users listing. */
router.get('/', getAllAddress);
router.get('/:addressUuid', getOneAddress);
router.post('/', createAddress);
router.put('/:addressUuid', updateAddress);
router.delete('/:addressUuid', deleteAddress);

module.exports = router;
