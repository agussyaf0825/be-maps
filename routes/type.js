const express = require('express');
const router = express.Router();
const { getAllType } = require('../controllers/type')

/* GET users listing. */
router.get('/', getAllType);

module.exports = router;
