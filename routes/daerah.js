const express = require('express');
const router = express.Router();
const { getAllProvinsi, getIdProvinsi } = require('../controllers/daerah/provinsi')
const { getAllKotaKabupaten, getIdKotaKabupaten } = require('../controllers/daerah/kotaKabupaten')
const { getAllKecamatan, getIdKecamatan } = require('../controllers/daerah/kecamatan')
const { getAllkelurahan, getIdkelurahan } = require('../controllers/daerah/kelurahan')

/* Provinsi */
router.get('/provinsi', getAllProvinsi);
router.get('/provinsi/:id', getIdProvinsi);
/* kota Kabupaten */
router.get('/kota-kabupaten', getAllKotaKabupaten);
router.get('/kota-kabupaten/:id', getIdKotaKabupaten);
/* kecamatan */
router.get('/kecamatan', getAllKecamatan);
router.get('/kecamatan/:id', getIdKecamatan);
/* kelurahan */
router.get('/kelurahan', getAllkelurahan);
router.get('/kelurahan/:id', getIdkelurahan);

module.exports = router;
